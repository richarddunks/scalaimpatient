1: [The signum of a number is 1 if the number is positive, –1 if it is negative, and 0 if it is zero. Write a function that computes this value.]

def signum(num: Int) = {
  if (num > 0)
    1
  else if (num < 0)
    -1
  else
    0
}

2: [What is the value of an empty block expression {}? What is its type?]

Value is (), type is Unit.

3: [Come up with one situation where the assignment x = y = 1 is valid in Scala. (Hint: Pick a suitable type for x.)]

If x is of type Unit, then x = y = 1 is valid, as the return value for the assignment y = 1 is Unit.

4: [Write a Scala equivalent for the Java loop
for (int i = 10; i >= 0; i--) System.out.println(i);]

for (i <- 10 to 0 by -1)
  println(i)

5: [Write a procedure countdown(n: Int) that prints the numbers from n to 0.]

def countdown(n: Int) {
  for (i <- n to 0 by -1)
    println(i)
}

6: [Write a for loop for computing the product of the Unicode codes of all letters in a string. For example, the product of the characters in "Hello" is 9415087488L]

val str = "Hello"
var sum = BigInt(1)
for (i <- 0 until str.length) {
  sum *= str.codePointAt(i)
}
println(sum)

7: [Solve the preceding exercise without writing a loop. (Hint: Look at the StringOps Scaladoc.)]

"Hello".map(i => BigInt(i)).product

8: [Write a function product(s : String) that computes the product, as described in the preceding exercises.]

def product(s: String) = {
  s.map(i => BigInt(i)).product
}

9: [Make the function of the preceding exercise a recursive function.]

def productRecursive(str: String) : BigInt = {
	if (str.length == 0) 1
	else productRecursive(str.takeRight(str.length-1)) * str.codePointAt(0)
}

10: [Write a function that computes x^n, where n is an integer. Use the following recursive definition:
x^n = y^2 if n is even and positive, where y = x^(n / 2)
x^n = x·x^(n – 1) if n is odd and positive
x^0 = 1
x^n = 1 / x^–n if n is negative
Don’t use a return statement.]

def power(x: Double, n : Int): Double = {
  var xn = 0.0
  if ((n > 0) && (n % 2 == 0)) {
    val y = power(x, n / 2)
    xn = y * y
  }
  else if ((n > 0) && (n % 2 != 0))
    xn = x * power(x, n - 1)
  else if (n == 0)
    xn = 1
  else if (n < 0) {
    xn = 1 / power(x, -n)
  }
  xn
}



