1: [Extend the following BankAccount class to a CheckingAccount class that charges $1 for every deposit and withdrawal.
class BankAccount(initialBalance: Double) {
  private var balance = initialBalance
  def deposit(amount: Double) = { balance += amount; balance }
  def withdraw(amount: Double) = { balance -= amount; balance }
}]

class BankAccount(initialBalance: Double) {
  private var balance = initialBalance
  
  def deposit(amount: Double) = { 
    balance += amount; balance 
  }
  
  def withdraw(amount: Double) = { 
    balance -= amount; balance 
  }
  
  override def toString = balance.toString
}

class CheckingAccount(initialBalance: Double) 
  extends BankAccount(initialBalance) {
  
  override def deposit(amount: Double) = {
    super.deposit(amount - 1.0)
  }
  
  override def withdraw(amount : Double) = {
    super.withdraw(amount + 1.0)
  }
}

object Main extends App {
  val account = new CheckingAccount(500.0)
  
  println(account)
  
  account.deposit(100)
  
  println(account)
  
  account.withdraw(50)
  
  println(account)
}

2: [Extend the BankAccount class of the preceding exercise into a class SavingsAccount that earns interest every month (when a method earnMonthlyInterest is called)
and has three free deposits or withdrawals every month. Reset the transaction
count in the earnMonthlyInterest method.]

class BankAccount(initialBalance: Double) {
  private var balance = initialBalance
  
  def deposit(amount: Double) = { 
    balance += amount; balance 
  }
  
  def withdraw(amount: Double) = { 
    balance -= amount; balance 
  }
  
  override def toString = balance.toString
}

class SavingsAccount(initialBalance: Double) 
  extends BankAccount(initialBalance) {
  var transactions = 0
  
  def earnMonthlyInterest(percent: Double) = {
    transactions = 0
    // In hindsight, base class should have had a protected field
    val currentBalance = super.deposit(0.0)
    super.deposit(currentBalance * (percent / 100.0))
  }
  
  override def deposit(amount: Double) = {
    transactions += 1
    if (transactions <= 3)
      super.deposit(amount)
    else 
      super.deposit(amount - 1.0)
  }
  
  override def withdraw(amount : Double) = {
    transactions += 1
    if (transactions <= 3)
      super.withdraw(amount)
    else
      super.withdraw(amount + 1.0)
  }
}

object Main extends App {
  val account = new SavingsAccount(500.0)
  
  println(account)
  
  account.deposit(100)
  
  println(account)
  
  account.withdraw(50)
  
  println(account)
  
  account.deposit(50)
  
  println(account)
  
  account.deposit(50)
  
  println(account)
  
  account.earnMonthlyInterest(5.0)
  
  println(account)
}

3: [Consult your favorite Java or C++ textbook that is sure to have an example of a toy inheritance hierarchy, perhaps involving employees, pets, graphical
shapes, or the like. Implement the example in Scala.

abstract class Shape(protected var _xPos: Double = 0.0, 
  protected var _yPos: Double = 0.0) {
  
  def area : Double
  
  def moveTo(xPos: Double, yPos: Double) {
    _xPos = xPos; _yPos = yPos
  }
  
  override def equals(other: Any) = {
    val that = other.asInstanceOf[Shape]
    if (that == null) false
    else
      _xPos == that._xPos && _yPos == that._yPos
  }
  
  override def toString = "Shape: Position: (" + _xPos + ", " + _yPos + ")"
}

class Circle(_xPos: Double = 0.0,
  _yPos: Double = 0.0,
  protected var _radius: Double = 0.0) extends Shape(_xPos, _yPos) {
  
  def area = {
    math.Pi * math.pow(_radius, 2.0)
  }
  
  def circumference = {
    2.0 * math.Pi * _radius;
  }
  
  final override def equals(other: Any) = {
    val that = other.asInstanceOf[Circle]
    if (that == null) false
    else
      super.equals(other) && _radius == that._radius
  }
  
  override def toString = "Circle: Center: (" + _xPos + ", " + _yPos + ") "
}

class Rect(_xPos: Double = 0.0,
    _yPos: Double = 0.0,
    protected var _xLower: Double = 0.0,
    protected var _yLower: Double = 0.0) extends Shape(_xPos, _yPos) {
  
  def area = {
    (_xLower - _xPos) * (_yPos - _yLower)
  }
 
  override def moveTo(xPos: Double, yPos: Double) {
    _xLower += xPos - _xPos
    _yLower += yPos - _yPos
    super.moveTo(xPos, yPos)
  }
  
  final override def equals(other: Any) = {
    val that = other.asInstanceOf[Rect]
    if (that == null) false
    else
      super.equals(other) && _xLower == that._xLower && _yLower == that._yLower
  }
  
  override def toString = "Rectangle: Upper Left: (" + 
    _xPos + ", " + _yPos + "). Lower Right: (" + _xLower + ", " + _yLower + ")";
}

object Main extends App {
  val circle = new Circle(1.0, 2.0, 3.0)
  println(circle)
  circle.moveTo(2.0, 1.0)
  println(circle)
  println(circle.area)
  
  val rect = new Rect(0.0, 1.0, 1.0, 0.0)
  println(rect)
  rect.moveTo(2.0, 3.0)
  println(rect)
  println(rect.area)
}

4: [Deﬁne an abstract class Item with methods price and description. A SimpleItem is an item whose price and description are speciﬁed in the constructor. Take
advantage of the fact that a val can override a def. A Bundle is an item that contains other items. Its price is the sum of the prices in the bundle. Also provide a mechanism for adding items to the bundle and a suitable description method.]

import scala.collection.mutable.ArrayBuffer

abstract class Item {
  def price : Double
  
  def description : String
}

class SimpleItem(val price: Double, val description: String) extends Item {
}

class Bundle(val description: String) {
  private val items = new ArrayBuffer[Item]
  
  def addItem(item: Item) {
    items += item
  }
  
  def price : Double = {
    var sum = 0.0
    items.foreach(sum += _.price)
    sum
  }
}

object Main extends App {
  val item1 = new SimpleItem(450.0, "iPhone")
  println(item1.price)
  println(item1.description)
  
  val item2 = new SimpleItem(600.0, "iPad")
  println(item2.price)
  println(item2.description)
  
  val bundle = new Bundle("Apple Store")
  bundle.addItem(item1)
  bundle.addItem(item2)
  println(bundle.description)
  println(bundle.price)
}

5: [Design a class Point whose x and y coordinate values can be provided in a constructor. Provide a subclass LabeledPoint whose constructor takes a label value and x and y coordinates, such as new LabeledPoint("Black Thursday", 1929, 230.07)]

class Point(val x: Double, val y: Double) {
}

class LabeledPoint(val label: String, x: Double, y: Double) extends Point(x, y) {
}

object Main extends App {
  val labeledPoint = new LabeledPoint("Black Thursday", 1929, 230.07)
}

6: [Deﬁne an abstract class Shape with an abstract method centerPoint and subclasses
Rectangle and Circle. Provide appropriate constructors for the subclasses and override the centerPoint method in each subclass.]

abstract class Shape(protected var _xPos: Double = 0.0, 
  protected var _yPos: Double = 0.0) {
  
  def centerPoint : (Double, Double)
  
  override def toString = "Shape: Position: (" + _xPos + ", " + _yPos + ")"
}

class Circle(_xPos: Double = 0.0,
  _yPos: Double = 0.0,
  protected var _radius: Double = 0.0) extends Shape(_xPos, _yPos) {
  
  def centerPoint = {
    (_xPos, _yPos)
  }
}

class Rect(_xPos: Double = 0.0,
    _yPos: Double = 0.0,
    protected var _xLower: Double = 0.0,
    protected var _yLower: Double = 0.0) extends Shape(_xPos, _yPos) {
  
  def centerPoint = {
    (_xLower + ((_xPos - _xLower) / 2.0), 
     _yLower + ((_yPos - _yLower) / 2.0))
  }
}

object Main extends App {
  val circle = new Circle(1.0, 2.0, 3.0)
  println(circle)
  println(circle.centerPoint)
  
  val rect = new Rect(0.0, 1.0, 1.0, 0.0)
  println(rect)
  println(rect.centerPoint)
}

7: [Provide a class Square that extends java.awt.Rectangle and has three constructors: one that constructs a square with a given corner point and width,
one that constructs a square with corner (0, 0) and a given width, and one that constructs a square with corner (0, 0) and width 0.]

class Square(x: Int, y: Int, width: Int) extends java.awt.Rectangle(x, y, width, width) {
  def this(width: Int) {
    this(0, 0, width)
  }
  
  def this() {
    this(0, 0, 0)
  }
}


object Main extends App {
  val sq1 = new Square(5, 5, 10)
  val sq2 = new Square(10)
  val sq3 = new Square
}

8: [Compile the Person and SecretAgent classes in Section 8.6, “Overriding Fields,”
on page 89 and analyze the class ﬁles with javap. How many name ﬁelds are there? How many name getter methods are there? What do they get? (Hint: Use the -c and -private options.)]

Person: 1 name field, 1 getter method for name field
SecretAgent: 1 name field, 1 toString field, 1 name getter method, 1 toString getter method 

9: [In the Creature class of Section 8.10, “Construction Order and Early Deﬁnitions,” on page 92, replace val range with a def. What happens when you also use a def in the Ant subclass? What happens when you use a val in the subclass?
Why?]

For the first case (def in Ant subclass), you end up with an array of size 2. For the second case (val in Ant subclass), you end up with an array of size 0. This is because of construction order, specifically in the latter case, the range member has not been initialised to 2 at the point at which the array env is created, so is 0, whereas in the former case, the method range is called when array env is created, which returns 2.

10: [The ﬁle scala/collection/immutable/Stack.scala contains the deﬁnition class Stack[A] protected (protected val elems: List[A]). Explain the meanings of the protected keywords. (Hint: Review the discussion
of private constructors in Chapter 5.)]

The first protected keywork means that the primary constructor is protected (i.e. accessible only to derived classes). The second keywork means that the parameter val is protected.

