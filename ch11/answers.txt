1: [According to the precedence rules, how are 3 + 4 -> 5 and 3 -> 4 + 5 evaluated?]

They are evaluated as (3 + 4) -> 5 and (3 -> 4) + 5

2: [The BigInt class has a pow method, not an operator. Why didn’t the Scala library designers choose ** (as in Fortran) or ^ (as in Pascal) for a power operator?]

For a power operator to work in an intuitive fashion (from a mathematical perspective), it needs to have higher precedence than any of the arithmetic operators. For example: 3 * 4 ^ 6 or 3 * 4 ** 6 should be construed as (4 ^ 6) * 3 and 3 * (4 ** 6) respectively in order to be intuitive. However, in Scala ** is on the same precedence level as *, and ^ is a lower precedence level than any arithmetic operator, so pow() actually makes more sense, as it has a higher precedence than any of the operator characters.

3: [Implement the Fraction class with operations + - * /. Normalize fractions, for
example turning 15/–6 into –5/3. Divide by the greatest common divisor,
like this:
class Fraction(n: Int, d: Int) {
  private val num: Int = if (d == 0) 1 else n * sign(d) / gcd(n, d);
  private val den: Int = if (d == 0) 0 else d * sign(d) / gcd(n, d);
  override def toString = num + "/" + den
  def sign(a: Int) = if (a > 0) 1 else if (a < 0) -1 else 0
  def gcd(a: Int, b: Int): Int = if (b == 0) abs(a) else gcd(b, a % b)
  ...
}]

object Fraction {
  def apply(n: Int, d: Int) = new Fraction(n, d)
}

class Fraction(n: Int, d: Int) {
  private val num: Int = if (d == 0) 1 else n * sign(d) / gcd(n, d)
  private val den: Int = if (d == 0) 0 else d * sign(d) / gcd(n, d)
  override def toString = num + "/" + den
  def sign(a: Int) = if (a > 0) 1 else if (a < 0) -1 else 0
  def gcd(a: Int, b: Int): Int = if (b == 0) math.abs(a) else gcd(b, a % b)
  
  def +(rhs: Fraction) : Fraction = {
    val newNum = num + rhs.num
    val newDen = den + rhs.den
    val newGcd = gcd(newNum, newDen)
    Fraction(newNum / newGcd, newDen / newGcd)
  }
  
  def -(rhs: Fraction) : Fraction = {
    val newNum = num - rhs.num
    val newDen = den - rhs.den
    val newGcd = gcd(newNum, newDen)
    Fraction(newNum / newGcd, newDen / newGcd)
  }
   
  def *(rhs: Fraction) : Fraction = {
    val newNum = num * rhs.num
    val newDen = den * rhs.den
    val newGcd = gcd(newNum, newDen)
    Fraction(newNum / newGcd, newDen / newGcd)
  }
  
  def /(rhs: Fraction) : Fraction = {
    val newNum = num / rhs.num
    val newDen = den / rhs.den
    val newGcd = gcd(newNum, newDen)
    Fraction(newNum / newGcd, newDen / newGcd)
  }
}

object Main extends App {   
  val frac1 = Fraction(15, -6)
  println(frac1.toString)
  
  val frac2 = Fraction(2, 3)
  println(frac1 * frac2)
}

4: [Implement a class Money with ﬁelds for dollars and cents. Supply +, - operators as well as comparison operators == and <. For example, Money(1, 75) + Money(0, 50) == Money(2, 25) should be true. Should you also supply * and / operators? Why
or why not?]

object Money {
  def apply(dollars: Int, cents: Int) = new Money(dollars, cents)
}

class Money(val dollars: Int, val cents: Int) {
  private def toCents = dollars * 100 + cents
  private def fromCents(cents: Int) = Money(cents / 100, cents % 100)
  
  def +(rhs: Money) = {
    val newCents = toCents + rhs.toCents
    fromCents(newCents)
  }
  
  def -(rhs: Money) = {
    val newCents = toCents - rhs.toCents
    fromCents(math.round(newCents).toInt)
  }
  
  def *(rhs: Double) = {
    val newCents = toCents * rhs
    fromCents(math.floor(newCents).toInt)
  }
  
  def /(rhs: Double) = {
    val newCents = toCents / rhs
    fromCents(math.floor(newCents).toInt)
  }
  
  def ==(rhs: Money) = {
    (dollars == rhs.dollars && cents == rhs.cents)
  }
  
  def <(rhs: Money) = {
    (dollars < rhs.dollars || (dollars == rhs.dollars && cents < rhs.cents))
  }
  
  override def toString = "$" + dollars + "." + cents
}

object Main extends App {   
  val money1 = Money(2, 25)
  val money2 = Money(1, 50)
  println(money1)
  println(money2)
  println(money1 + money2)
  println(money1 - money2)
  println(money1 * 2.0)
  println(money1 / 2.0)
  println(Money(1, 75) + Money(0, 50) == Money(2, 25))
}

Yes you should provide * and / operators, but not in the sense of multiplying Money * Money or dividing Money / Money. That wouldn't make semantic sense - it is not intuitive to say 2 dollars 20 cents * 5 dollars 50 cents = x dollars? Instead, multiplication and division should be Money * Scalar or Money / Scalar, or in other words multiplied or divided by a single scalar value type.

5: [Provide operators that construct an HTML table. For example,
Table() | "Java" | "Scala" || "Gosling" | "Odersky" || "JVM" | "JVM, .NET"
should produce <table><tr><td>Java</td><td>Scala</td></tr><tr><td>Gosling...]

object Table {
  def apply(cell: String = "<table><tr>") = {
    new Table(cell)
  }
  
}

class Table(var _cell: String) {  
  def |(cell: String) = {
    _cell += ("<td>" + cell + "</td>")
    println(_cell)
    _cell
  }
  
  def ||(cell: String) = {
    if (cell.isEmpty())
      _cell += "</tr></table>"
    else 
      _cell += ("</tr><tr><td>" + cell + "</td>")
    
    println(_cell)
    _cell
  }
  
}

object Main extends App {
  implicit def stringToTable(cell: String) = Table(cell)

  Table() | "Java" | "Scala" || "Gosling" | "Odersky" || "JVM" | "JVM, .NET" || ""
}

6: [Provide a class ASCIIArt whose objects contain ﬁgures such as
 /\_/\   
( ' ' )  
(  -  ) 
 | | |  
(__|__) 
Supply operators for combining two ASCIIArt ﬁgures horizontally
 /\_/\     -----
( ' ' )  / Hello \
(  -  ) <  Scala |
 | | |   \ Coder /
(__|__)    -----
or vertically. Choose operators with appropriate precedence.]

class ASCIIArt(var _fig: String) {
  def |(fig: ASCIIArt) = {
    val figLinePairs = _fig.split("\n").zip(fig.toString.split("\n"))
    _fig = ""
    for ((origFigLine, figLine) <- figLinePairs) _fig += (origFigLine + figLine + "\n")
    _fig
  }
  
  def -(fig: ASCIIArt) = {
    _fig += "\n" + fig
    _fig
  }
  
  override def toString = _fig
}

object ASCIIArt {
  def scalaDog = {
    new ASCIIArt(""" /\_/\ """ + "\n" +
                 """( ' ' )""" + "\n" +
                 """(  -  )""" + "\n" +
                 """ | | | """ + "\n" +
                 """(__|__)""")
  }
  
  def scalaCoder = {
    new ASCIIArt("""   -----  """ + "\n" +
                 """ / Hello \""" + "\n" +
                 """< Scala  |""" + "\n" +
                 """ \ Coder /""" + "\n" +
                 """   -----  """)
  }
}

object Main extends App {
  println(ASCIIArt.scalaDog | ASCIIArt.scalaCoder)
  println(ASCIIArt.scalaDog - ASCIIArt.scalaCoder)
}

7: [Implement a class BitSequence that stores a sequence of 64 bits packed in a Long value. Supply apply and update operators to get and set an individual bit.]

object BitSequence {
  def apply(bit: Int, value: Boolean) = {
    new BitSequence(bit, value)
  }
}

class BitSequence(bit: Int, value: Boolean) { 
  implicit def bool2int(b: Boolean) = if (b) 1 else 0
  
  var packed: Long = 0L
  packed |= (value << bit)
  
  def apply(bit: Int) : Boolean = {
    (packed & (1L << bit)) != 0
  }
  
  def update(bit: Int, value: Boolean) = {
    packed |= (value << bit)
  }
  
  override def toString() = packed.toHexString
}

object Main extends App {
  var bitSeq = BitSequence(6, true)
  println(bitSeq)
  bitSeq(5) = true
  println(bitSeq)
  val bitValue1 = bitSeq(5)
  println(bitValue1)
  val bitValue2 = bitSeq(7)
  println(bitValue2)
}

8: [Provide a class Matrix—you can choose whether you want to implement 2 × 2
matrices, square matrices of any size, or m × n matrices. Supply operations +
and *. The latter should also work with scalars, for example mat * 2. A single
element should be accessible as mat(row, col).]

import scala.collection.JavaConversions._

object Matrix {
  def random(m: Int, n: Int) = {
    new Matrix(Array.fill(m, n) { util.Random.nextInt(9) })
  }

  def apply(m: Int, n: Int) = {
    new Matrix(Array.ofDim[Double](m, n))
  }
}

class Matrix(val _matrix: Array[Array[Double]]) {
  def apply(i: Int, j: Int) = {
    _matrix(i)(j)
  }

  def +(rhs: Matrix): Matrix = {
    val newMatrix = Matrix(_matrix.size, _matrix(0).size)
    for (i <- 0 until _matrix.size) {
      for (j <- 0 until _matrix(0).size) {
        newMatrix._matrix(i)(j) = _matrix(i)(j) + rhs._matrix(i)(j)
      }
    }
    new Matrix(newMatrix._matrix)
  }

  def *(rhs: Matrix) = {
    if (_matrix(0).size != rhs._matrix.size)
      None
    else {
      val newMatrix = Matrix(_matrix.size, _matrix(0).size)
      for (i <- 0 until _matrix.size) {
        for (j <- 0 until rhs._matrix(0).size) {
          for (k <- 0 until _matrix(0).size) {
            newMatrix._matrix(i)(j) = _matrix(i)(k) + rhs._matrix(k)(j)
          }
        }
      }
      new Matrix(newMatrix._matrix)
    }
  }

  def *(scalar: Double): Matrix = {
    new Matrix(_matrix.map(_.map(_ * 5)))
  }

  override def toString = {
    val rowStrings = for (row <- _matrix)
      yield row.mkString("[", ", ", "]")
    rowStrings.mkString("", "\n", "\n")
  }
}

object Main extends App {
  val mat1 = Matrix.random(3, 3)
  val mat2 = Matrix.random(3, 3)

  println(mat1)
  println(mat2)
  println(mat1 + mat2)
  println(mat1 * mat2)
  println(mat1 * 5)
  println(mat1(2, 1))
}

9: [Deﬁne an unapply operation for the RichFile class that extracts the ﬁle path,
name, and extension. For example, the ﬁle /home/cay/readme.txt has path /home/cay,
name readme, and extension txt.]

import java.io.File
import io.Source

// Using RichFile implementation from Chapter 21
class RichFile(val from: File) {
  def read = Source.fromFile(from.getPath).mkString
}

object RichFile {
  def unapply(input: RichFile) = {    
    val name = input.from.getName
    Some(input.from.getParent, 
        name.substring(0, name.lastIndexOf(".")), 
        name.substring(name.lastIndexOf(".") + 1))
  }
}

object Main extends App {
  val RichFile(path, name, extension) = new RichFile(new File("/home/cay/readme.txt"))
  
  println(path)
  println(name)
  println(extension)
}

10: [Deﬁne an unapplySeq operation for the RichFile class that extracts all path
segments. For example, for the ﬁle /home/cay/readme.txt, you should produce a
sequence of three segments: home, cay, and readme.txt.]

import java.io.File
import io.Source

// Using RichFile implementation from Chapter 21
class RichFile(val from: File) {
  def read = Source.fromFile(from.getPath).mkString
}

object RichFile {  
  def unapplySeq(input: RichFile): Option[Seq[String]] = {
    Some(input.from.getPath.split("""/""").drop(1))
  }
}

object Main extends App {
  val RichFile(path, name, extension) = new RichFile(new File("/home/cay/readme.txt"))
  
  println(path)
  println(name)
  println(extension)
}

